﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SocialPlatforms.Impl;

public class EnemyScript : MonoBehaviour
{
    public bool IsAlive;
    public float EnemySpeed = 1;
    public float BridgeTime = 3;
    public int EnemyId;
    public int EnemyDeathPoint;

    Animator Animator;
    AudioSource AudioSource;

    private void Awake()
    {
        Animator = GetComponent<Animator>();
        AudioSource = GetComponent<AudioSource>();
    }
    void Start()
    {
        Animator.Play("Enemy_spawn", 0, 0);
        IsAlive = true;
        AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.JellyWalk);
        AudioSource.Play();
    }
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.F1))
            KillThemAll();
    }

    Sequence walkingSequence;
    internal void MoveTo(Transform bridgePosition, Transform wallPosition)
    {
        walkingSequence = DOTween.Sequence();

        walkingSequence.Append(transform.DOMove(bridgePosition.position, Vector3.Distance(transform.position, bridgePosition.position) / EnemySpeed).SetEase(Ease.Linear));
        walkingSequence.Join(transform.DORotateQuaternion(Quaternion.LookRotation(transform.position - bridgePosition.position), 0.5f).SetEase(Ease.Linear));

        walkingSequence.Append(transform.DOMove(wallPosition.position, BridgeTime).SetEase(Ease.Linear)).OnComplete(() => OnComplete());
        walkingSequence.Join(transform.DORotateQuaternion(Quaternion.LookRotation(bridgePosition.position - wallPosition.position), 0.5f).SetEase(Ease.Linear));

    }

    public void OnComplete()
    {
        GameManager.instance.EnemyReachedToEnd();
        Kill(true);

    }
    public void Kill(bool isCrashed)
    {
        if (!isCrashed)
        {
            GameManager.instance.UpdateScore(EnemyDeathPoint);

            StartCoroutine(DieSequence(() =>
            {
                SpawnManager.Instance.EnemyCharacterDestroyed(this);
                Destroy(gameObject);

            }));
        }
        else
        {
            StartCoroutine(ExplodeSequence(() =>
            {
                SpawnManager.Instance.EnemyCharacterDestroyed(this);
                Destroy(gameObject);

            }));
        }
        
    }
    public void KillThemAll()
    {
        StartCoroutine(EndGameSequence(() =>
        {
            SpawnManager.Instance.EnemyCharacterDestroyed(this);
            Destroy(gameObject);

        }));
    }
    IEnumerator DieSequence(System.Action func, bool isSoundActive = true)
    {
        IsAlive = false;
        walkingSequence?.Kill();
        if (isSoundActive)
        {
            AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.EnemyExplode);
            AudioSource.Play();
        }
        Animator.Play("Enemy_transformation", 0, 0);
        yield return new WaitForSeconds(2f);
        func?.Invoke();
    }
    IEnumerator ExplodeSequence(System.Action func, bool isSoundActive = true)
    {
        IsAlive = false;
        walkingSequence?.Kill();
        if (isSoundActive)
        {
            AudioSource.clip = SoundManager.Instance.GetSound(SoundTypes.EnemyExplode);
            AudioSource.Play();
        }
        Animator.Play("Enemy_hit", 0, 0);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);
        func?.Invoke();
    }IEnumerator EndGameSequence(System.Action func)
    {
        IsAlive = false;
        walkingSequence?.Kill();

        Animator.Play("Enemy_endgame", 0, 0);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);
        func?.Invoke();
    }
    private void OnDestroy()
    {
        walkingSequence?.Kill();
    }






}
