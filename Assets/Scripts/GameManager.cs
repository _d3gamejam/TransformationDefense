﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public TextMeshPro ScoreText;
    public TextMeshPro HighScoreText;
    public int Score;
    public int HighScore
    {
        get
        {
            return PlayerPrefs.GetInt("highscore", 0);
        }

        set
        {
            if (value > HighScore)
            {
                PlayerPrefs.SetInt("highscore", value);
            }
            
        }
    }
    public bool isGameActive;
    public static GameManager instance { get; private set; }

    private void Awake()
    {
        instance = this;
        if (!SoundManager.Instance)
            SceneManager.LoadScene(1, LoadSceneMode.Additive);
    }

    public void UpdateScore(int EnemyDeathPoint)
    {
        Score += EnemyDeathPoint;
        HighScore = Score;
        HighScoreText.text = HighScore.ToString();
        ScoreText.text = Score.ToString();
    }

    public void Start()
    {
        HighScoreText.text = HighScore.ToString();
    }

    public void StartGame(int difficulty)
    {
        isGameActive = true;
        Score = 0;
        
        StartCoroutine(SpawnManager.Instance.SpawnTarget());
    }

    private void Update()
    {
        
        
    }


    public void EnemyReachedToEnd()
    {
        CannonController.instance.CannonIsHit();
    }

    public void GameOver()
    {
        Debug.Log("Game Over!");
        //Time.timeScale = 0;
        isGameActive = false;
        SpawnManager.Instance.ClearAllEnemies();
        
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }


}

